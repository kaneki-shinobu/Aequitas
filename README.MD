[![build status](https://gitlab.com/kaneki-shinobu/Aequitas/badges/master/build.svg)](https://gitlab.com/kaneki-shinobu/Aequitas/commits/master)

[![coverage report](https://gitlab.com/kaneki-shinobu/Aequitas/badges/master/coverage.svg)](https://gitlab.com/kaneki-shinobu/Aequitas/commits/master)

Contract Address: [0x1EB96d45C1f8FfB2c583262cE5E536D0B38F160d](https://etherscan.io/address/0x1EB96d45C1f8FfB2c583262cE5E536D0B38F160d)

Use [abi.json](abi.json) to interact with the contract.

Or visit the [website](https://kaneki-shinobu.gitlab.io/Aequitas/index.html) (requires MetaMask to interact with the contract).

View [detailed code coverage reports](https://kaneki-shinobu.gitlab.io/Aequitas/coverage/lcov-report/).

Read the [white paper](https://kaneki-shinobu.gitlab.io/Aequitas/whitepaper.pdf).

## Aequitas

Aequitas is an experimental cryptocurrency that replicates Bitcoin's issuance/reward schedule and blocktime, but replaces wasteful mining with the provision of Ether as capital in exchange for Aequitas tokens (AQT).

## Motivation

This project aims to investigate Bitcoin's resounding success and popularity in the world of cryptocurrency, and attempts to replicate it while avoiding the scaling issues that plague it.

## Mining AQT

AQT is mined, with limited supply, but perhaps not quite in the same way as most are used to. Hardware mining equipment is not required.

Easy Way (via MetaMask):

Visit the token website using Chrome, with MetaMask installed, and the contributing account listed in it. Select the contributing account and click on Mine to contribute. This value is not customizable. MetaMask will prompt you to sign the transaction. Click on "Receive Payout" 10 minutes later and sign the transaction, and you should receive the tokens in your account shortly.

Otherwise:

Simply call the mine() function of the contract with no arguments, with a value of no less than 0.05 ether. Your account will be credited with AQT when you call either updateAccount() or mine() again 10 minutes later, in proportion to how much you have contributed over the 10 minute block. Please see the white paper for more details.

Be advised before contributing that the minted coins are the product. They do not serve any practical purpose, and are not intended to do so in any sense, except in the same way that Bitcoin does. Our intentions in the creation of this cryptocurrency are twofold: to fund our research team's ongoing work, and to investigate the extent to which Bitcoin's market supply variables are the cause of its success. We will introduce the cryptocurrency to the community via /r/ethtrader on reddit, and observe if/how it propagates from there.

All Ether contributions will go towards funding our research team's ongoing experiments in cryptocurrencies in Tokyo, Japan.

## Tests

Run the below command to start testrpc and run truffle's tests at the same time.
```
npm test
```

## License
MIT License

Copyright 2017 Kaneki Shinobu (金城 清信)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.