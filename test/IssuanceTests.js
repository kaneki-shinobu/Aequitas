/**
 * @author Kaneki Shinobu (金城 清信) <kaneki.shinobu@gmail.com>
 * @license
 * The MIT License (MIT)
 * Copyright (c) 2017 Kaneki Shinobu (金城 清信)
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

let Aequitas = artifacts.require("./Aequitas.sol");
let CapitalMiningToken = artifacts.require("./CapitalMiningToken.sol");

const Helpers = require('./helpers/Helpers');
let timeTravel = Helpers.timeTravel;
let mineBlock = Helpers.mineBlock;
let getNewCapitalMiningContract = Helpers.getNewCapitalMiningContract;
let checkContractProperty = Helpers.checkContractProperty;

contract('IssuanceTest', function (accounts) {
    it('should have issuance schedule identical to that of BTC, but for the purposes of this test, it is ' +
        '210,000 times faster, and hence total issued coin will similarly be fractionally smaller', function () {
            let instance = getNewCapitalMiningContract(accounts[0]);
            let address;
            let rewardHalveningRate = 1;
            let resolveContract = function () {
                return instance.then(function (contract) {
                    address = contract.address;
                    instance = contract;
                }).then(function() {
                    return instance.rewardReductionRate.call({from: accounts[0]});
                }).then(function(rhr) {
                    rewardHalveningRate = rhr;
                });
            };
            let promiseLoop = resolveContract();
            let rewardEpoch = function () {
                let init_mine = instance.mine({
                    from: accounts[0], value: web3.toWei(0.1, "ether")
                }).then(function () {
                    return instance.simulatedBlockNumber.call({from: accounts[0]});
                }).then(function (simulatedBlockNumber) {
                    //console.log("Block: " + simulatedBlockNumber);
                    return instance.rewardValue.call({from: accounts[0]});
                }).then(function (rewardValue) {
                    //console.log("Reward: " + rewardValue);
                    return timeTravel(600);
                }).then(function() {
                    return instance.updateAccount({from: accounts[0]});
                }).then(function () {
                    return instance.balanceOf(accounts[0], {from: accounts[0]});
                }).then(function (balance) {
                    //console.log(balance);
                });

                let loop = function () {
                    return instance.mine({from: accounts[0], value: web3.toWei(0.1, "ether")
                    }).then(function () {
                        return instance.simulatedBlockNumber.call({from: accounts[0]});
                    }).then(function (simulatedBlockNumber) {
                        //console.log("Block: " + simulatedBlockNumber);
                        return instance.rewardValue.call({from: accounts[0]});
                    }).then(function (rewardValue) {
                        //console.log("Reward: " + rewardValue);
                        return timeTravel(600);
                    }).then(function() {
                        return instance.updateAccount({from: accounts[0]});
                    }).then(function () {
                        return instance.balanceOf(accounts[0], {from: accounts[0]});
                    }).then(function (balance) {
                        //console.log(balance);
                    });
                };
                for (let i = 0; i < rewardHalveningRate - 1; i++) {
                    init_mine = init_mine.then(loop);
                }
                return init_mine.then(function(){
                    return instance.withdrawEther({from: accounts[0]});
                }).then(function() {
                    return instance.updateAccount({from: accounts[0]});
                });
            };
            // Run for each reward epoch
            for (let i = 0; i < 34; i++) {
                promiseLoop = promiseLoop.then(rewardEpoch);
            }

            let finalBTCBalance = 20999999.97690000;
            return promiseLoop.then(function () {
                return instance.balanceOf(accounts[0], {from: accounts[0]});
            }).then(function (balance) {
                let finalCoinBalance = balance / 100000000;
                //console.log("Final coin balance: " + finalCoinBalance);
                //console.log("Final Bitcoin balance: " + finalBTCBalance);
                //console.log("Comparing to Bitcoin balance: " + (finalCoinBalance * 210000)/rewardHalveningRate);
                assert.equal(finalCoinBalance * 210000 / rewardHalveningRate, finalBTCBalance, "Expected actual final supply is incorrect");
                return instance.totalSupply({from: accounts[0]});
            }).then(function (totalSupply) {
                //console.log(totalSupply);
                assert.equal(totalSupply, finalBTCBalance * rewardHalveningRate * 100000000 / 210000, "Expected total supply is incorrect");
                return instance.pendingPayouts(accounts[0], {from: accounts[0]});
            }).then(function (pendingPayout) {
                assert.equal(pendingPayout[1], 0, "Expected pendingPayout value is incorrect");
                return instance.withdrawEther({from: accounts[0]});
            }).then(function () {
                return web3.eth.getBalance(accounts[0]);
            }).then(function (balance) {
                assert.equal(balance > 0, true);
            })
        }
    );
});