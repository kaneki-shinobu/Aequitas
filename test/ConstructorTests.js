/**
 * @author Kaneki Shinobu (金城 清信) <kaneki.shinobu@gmail.com>
 * @license
 * The MIT License (MIT)
 * Copyright (c) 2017 Kaneki Shinobu (金城 清信)
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

let Aequitas = artifacts.require("./Aequitas.sol");

let canon_contract_properties = new Map();
const Helpers = require('./helpers/Helpers');
let checkContractProperty = Helpers.checkContractProperty;

canon_contract_properties.set("name", "Aequitas");
canon_contract_properties.set("decimals", 8);
canon_contract_properties.set("symbol", "AQT");
canon_contract_properties.set("version", "0.1");
canon_contract_properties.set("totalSupply", 0);
canon_contract_properties.set("simulatedBlockNumber", 0);
canon_contract_properties.set("rewardScarcityFactor", 2);
canon_contract_properties.set("rewardReductionRate", 210000);
canon_contract_properties.set("blockInterval", 600);
canon_contract_properties.set("rewardValue", 5000000000);

let error_message = "Attribute was set incorrectly; was actually: ";

for (let [attribute, answer] of canon_contract_properties) {
    contract('ConstructorSet' + attribute.charAt(0).toUpperCase() + attribute.slice(1) + 'Test', function (accounts) {
        it("Should set" + attribute + " correctly", function () {
            return checkContractProperty(Aequitas.new(), attribute, [], accounts[0], answer, error_message);
        });
    });
}