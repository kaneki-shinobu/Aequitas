/**
 * @author Kaneki Shinobu (金城 清信) <kaneki.shinobu@gmail.com>
 * @license
 * The MIT License (MIT)
 * Copyright (c) 2017 Kaneki Shinobu (金城 清信)
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */

let Aequitas = artifacts.require("./Aequitas.sol");
let CapitalMiningToken = artifacts.require("./CapitalMiningToken.sol");

const Helpers = require('./helpers/Helpers');
let timeTravel = Helpers.timeTravel;
let mineBlock = Helpers.mineBlock;
let checkContractProperty = Helpers.checkContractProperty;
contract('MultipleContributionsWithinSingleBlockAndFinalPayoutAfterTest', function (accounts) {
    it('should list 50000000 AQT as a final payout', function () {
        let instance = Aequitas.new({from: accounts[0]});
        let address;
        return instance.then(function (contract) {
            address = contract.address;
            instance = contract;
            // We execute a check on the pendingPayouts to ensure it is initialized correctly
            return instance.pendingPayouts(accounts[0], {from: accounts[0]});
        }).then(function (pendingPayout) {
            // All values of pendingPayout should be 0 at this point
            assert.equal(pendingPayout[0], 0, "Address should still be 0 at the beginning");
            assert.equal(pendingPayout[1], 0, "Block payout should still be 0 at the beginning");
            assert.equal(pendingPayout[2], 0, "Last contribution block number should still be 0 at the beginning");
            assert.equal(pendingPayout[3], 0, "Total personal block contribution should still be 0 at the beginning");
            // We now contribute to the contract
            return instance.mine({from: accounts[0], value: web3.toWei(0.1, "ether")});
        }).then(function () {
            // and then we look at our payout now
            return instance.pendingPayouts(accounts[0], {from: accounts[0]});
        }).then(function (pendingPayout) {
            // check if we have increased our initial pending payout
            assert.equal(pendingPayout[3], web3.toWei(0.1, "ether"), "Initial contribution is instantly updated");
            // it's worked, so we try making another contribution
            return instance.mine({from: accounts[0], value: web3.toWei(0.1, "ether")});
        }).then(function () {
            // and then we check our payout again
            return instance.pendingPayouts(accounts[0], {from: accounts[0]});
        }).then(function (pendingPayout) {
            // the payout is not instant as it's within the same simulated block, so pendingPayout should be increased
            assert.equal(pendingPayout[3], web3.toWei(0.2, "ether"), "Second contribution is instantly updated");
            return instance.balanceOf(accounts[0], {from: accounts[0]});
        }).then(function (balance) {
            // check that indeed nothing is paid out
            assert.equal(balance, 0, 'Both calls to mine were within the same block, so nothing is paid out yet.');
            return instance.pendingPayouts(accounts[0], {from: accounts[0]});
        }).then(function (pendingPayout) {
            assert.equal(pendingPayout[1], 5000000000, 'Pending payouts should be listed properly.');
            timeTravel(600);
        }).then(function () {
            return mineBlock();
        }).then(function() {
            return instance.mine({from: accounts[0], value: web3.toWei(0.1, "ether")});
        }).then(function() {
            return instance.updateAccount({from: accounts[0]});
        }).then(function () {
            return instance.pendingPayouts(accounts[0], {from: accounts[0]});
        }).then(function (pendingPayout) {
            // All values of pendingPayout should have been updated
            assert.equal(pendingPayout[0], accounts[0], "Address should now be " + accounts[0] + " at the end");
            assert.equal(pendingPayout[1], 5000000000, "Block payout should now be " + 5000000000 + " at the end");
            assert.equal(pendingPayout[2], 1, "Last contribution block number should now be " + 1 + " at the end");
            assert.equal(pendingPayout[3], web3.toWei(0.1, "ether"), "Total personal block contribution should now be " + web3.toWei(0.1, "ether") + " at the end");
            return instance.balanceOf(accounts[0], {from: accounts[0]});
        }).then(function (balance) {
            assert.equal(balance, 5000000000, 'Paid out amounts should be listed properly.');
        });
    });
});

contract('SenderBalanceOnContractInitializationTest', function (accounts) {
    it("should put 0 AQT in the first account", function () {
        return Aequitas.new({from: accounts[0]}).then(function (instance) {
            return instance.balanceOf.call(accounts[0], {from: accounts[0]});
        }).then(function (balance) {
            assert.equal(balance, 0, "Balance of sender address should be 0 after initialization (assuming 0 beforehand), but isn't");
        });
    });
});

contract('ContractOwnerOnCreationTest', function (accounts) {
    it('should test that the test contract is deployed by the correct address (default)', function () {
        return Aequitas.new({from: accounts[0]}).then(function (instance) {
            return instance.owner.call({from: accounts[0]});
        }).then(function (owner) {
            assert.equal(owner, accounts[0], 'Test owned by the wrong address');
        });
    });
});

contract('ContractBalanceIncreaseOnMineFunctionTest', function (accounts) {
    it('should put 10 eth in the contract account', function () {
        let instance = Aequitas.new({from: accounts[0]});
        let address;
        return instance.then(function (instance) {
            address = instance.address;
            return instance.mine({from: accounts[0], value: web3.toWei(1, "ether")});
        }).then(function () {
            return web3.eth.getBalance(address);
        }).then(function (balance) {
            assert.equal(balance, web3.toWei(1, "ether"), 'Balance of contract should be 10 after 10 eth is sent.');
        });
    });
});

contract('ConsecutiveTransactionTest', function (accounts) {
    it('should put 30 eth in the contract account', function () {
        let instance = Aequitas.new({from: accounts[0]});
        let address;
        return instance.then(function (contract) {
            address = contract.address;
            instance = contract;
            return instance.mine({from: accounts[0], value: web3.toWei(1, "ether")});
        }).then(function () {
            return instance.mine({from: accounts[1], value: web3.toWei(1, "ether")});
        }).then(function () {
            return instance.mine({from: accounts[2], value: web3.toWei(1, "ether")});
        }).then(function () {
            return web3.eth.getBalance(address);
        }).then(function (balance) {
            assert.equal(balance, web3.toWei(3, "ether"), 'Balance of contract should be 10 after 10 eth is sent.');
        });
    });
});

contract('ConcurrentTransactionTest', function (accounts) {
    it('should put 30 eth in the contract account', function () {
        let instance = Aequitas.new({from: accounts[0]});
        let address;
        return instance.then(function (instance) {
            address = instance.address;
            return Promise.all([
                instance.mine({from: accounts[0], value: web3.toWei(1, "ether")}),
                instance.mine({from: accounts[1], value: web3.toWei(1, "ether")}),
                instance.mine({from: accounts[2], value: web3.toWei(1, "ether")})
            ]);
        }).then(function () {
            return web3.eth.getBalance(address);
        }).then(function (balance) {
            assert.equal(balance, web3.toWei(3, "ether"), 'Balance of contract should be 3 after 3 eth is sent.');
        });
    });
});

contract('GetCreditedAQTFromAutoUpdateTest', function (accounts) {
    it('should put 50000000 AQT in the contract account due to automatic update', function () {
        let instance = Aequitas.new({from: accounts[0]});
        let address;
        return instance.then(function (contract) {
            address = contract.address;
            instance = contract;
            return instance.mine({from: accounts[0], value: web3.toWei(1, "ether")});
        }).then(function () {
            return timeTravel(600);
        }).then(function () {
            return instance.mine({from: accounts[0], value: web3.toWei(1, "ether")});
        }).then(function () {
            return instance.balanceOf.call(accounts[0], {from: accounts[0]});
        }).then(function (balance) {
            assert.equal(balance, 5000000000, 'Contract should pay out 1 block reward.');
        });
    });
});

contract('GetPendingAQTTest', function (accounts) {
    it('should list 5000000000 AQT as a pending payout', function () {
        let instance = Aequitas.new({from: accounts[0]});
        let address;
        return instance.then(function (contract) {
            address = contract.address;
            instance = contract;
            return instance.mine({from: accounts[0], value: web3.toWei(1, "ether")});
        }).then(function () {
            return instance.mine({from: accounts[0], value: web3.toWei(1, "ether")});
        }).then(function () {
            return instance.pendingPayouts.call(accounts[0], {from: accounts[0]});
        }).then(function (pendingPayout) {
            assert.equal(pendingPayout[1], 5000000000, 'Pending payout should be 5000000000');
        });
    });
});
contract('TransferOwnershipTest', function (accounts) {
    it('should transfer ownership properly', function () {
        let instance = Aequitas.new({from: accounts[0]});
        let final_contract = null;
        let address;
        return instance.then(function (contract) {
            address = contract.address;
            final_contract = contract;
            return checkContractProperty(instance, "owner", [], accounts[0], accounts[0], "Incorrect initial owner.");
        }).then(function () {
            return final_contract.transferOwnership(accounts[1], {from: accounts[0]});
        }).then(function () {
            return checkContractProperty(instance, "owner", [], accounts[0], accounts[1], "Incorrect final owner.");
        });
    });
});

contract('TransferOwnershipFailTest', function (accounts) {
    it('should fail to transfer ownership because caller is not owner', function () {
        let instance = Aequitas.new({from: accounts[0]});
        let final_contract = null;
        let address;
        return instance.then(function (contract) {
            address = contract.address;
            final_contract = contract;
            return checkContractProperty(instance, "owner", [], accounts[0], accounts[0], "Incorrect initial owner.");
        }).then(function () {
            return final_contract.transferOwnership(accounts[1], {from: accounts[1]});
        }).then(function () {
            assert(false);
        }).catch(function(error){
            assert.equal(error.message, "VM Exception while processing transaction: invalid opcode");
        });
    });
});



contract('SendFailTest', function (accounts) {
    it('should fail to send eth', function () {
        let instance = Aequitas.new({from: accounts[0]});
        let address;
        return instance.then(function (contract) {
            address = contract.address;
            instance = contract;
            return web3.eth.sendTransaction({from: accounts[0], to: address, value: web3.toWei("10", "Ether")});
        }).then(function () {
            assert(false, "An exception should be thrown when sending eth to fallback function");
            //return checkContractProperty("owner", [], accounts[0], accounts[1], "Incorrect final owner.");
        }).catch(function(error){
            assert.equal(error.message, "VM Exception while processing transaction: invalid opcode");
        });
    });
});

contract('FailLowContributionTest', function (accounts) {
    it('should fail contributions less than 50 finney', function () {
        let instance = Aequitas.new({from: accounts[0]});
        let address;
        return instance.then(function (contract) {
            address = contract.address;
            instance = contract;
            return instance.mine({from: accounts[0], value: web3.toWei(0.0499, "ether")});
        }).then(function () {
            assert(false, "An exception should be thrown when sending less than 50 finney");
            return instance.pendingPayouts.call(accounts[0], {from: accounts[0]});
        }).catch(function(error){
            assert.equal(error.message, "VM Exception while processing transaction: invalid opcode", "An exception should be thrown when sending less than 50 finney");
        });
    });
});
