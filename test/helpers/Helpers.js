/**
 * @author Kaneki Shinobu (金城 清信) <kaneki.shinobu@gmail.com>
 * @license
 * The MIT License (MIT)
 * Copyright (c) 2017 Kaneki Shinobu (金城 清信)
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
 */
let Aequitas = artifacts.require("./Aequitas.sol");
let CapitalMiningToken = artifacts.require("./CapitalMiningToken.sol");

exports.checkContractProperty = function (contract, property, args, caller, expected_result, assert_fail_message) {
    return contract.then(function (instance) {
        return instance[property].call(...args, {from: caller});
    }).then(function (result) {
        assert.equal(result, expected_result, assert_fail_message + result);
    });
};

exports.timeTravel = function (time) {
    return new Promise(function (resolve, reject) {
        web3.currentProvider.sendAsync({
            jsonrpc: "2.0",
            method: "evm_increaseTime",
            params: [time], // 86400 is num seconds in day
            id: new Date().getTime()
        }, function (err, result) {
            if (err) {
                return reject(err)
            }
            return resolve(result)
        });
    })
};

exports.mineBlock = function() {
    return new Promise(function (resolve, reject) {
        web3.currentProvider.sendAsync({
            jsonrpc: "2.0",
            method: "evm_mine"
        }, function (err, result) {
            if (err) {
                return reject(err)
            }
            return resolve(result)
        });
    })
};


exports.revert = function() {
    return new Promise(function (resolve, reject) {
        web3.currentProvider.sendAsync({
            jsonrpc: "2.0",
            method: "revert"
        }, function (err, result) {
            if (err) {
                return reject(err)
            }
            return resolve(result)
        });
    })
};

exports.getNewCapitalMiningContract = function(sender) {
    "use strict";

    return CapitalMiningToken.new(
        "Aequitas",             // name
        8,                      // decimals
        "AQT",                  // symbol
        "0.1",                  // version
        0,                      // initialAmount
        0,                      // simulatedBlockNumber
        2,                      // rewardScarcityFactor
        1,                     // rewardHalveningRate
        600,                    // blockInterval
        5000000000              // rewardValue)
        , {from: sender});
};