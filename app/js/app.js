window.addEventListener('load', function () {
    App = {
        web3Provider: null,
        contract: null,
        init: function () {
            return new Promise(App.initWeb3)
                .then(App.updateDisplay)
                .then(Display.display)
                .then(App.cleanupError)
                .catch(App.handleError);
        },

        initWeb3: function (resolve, reject) {
            if (typeof web3 !== 'undefined') {
                App.web3Provider = web3.currentProvider;
                web3 = new Web3(web3.currentProvider);
                return App.initContract(resolve, reject);
            } else {
                $('#contribution').prop('disabled', true);
                return reject('<strong>Warning</strong>: MetaMask was not detected on your browser. You will not be able to view contract information or access the Aequitas contract via this page until you install MetaMask.');
            }
        },

        initContract: function (resolve, reject) {
            App.bindEvents();
            if (!App.handleNetworkCheck()) {
                return reject('<strong>Warning</strong>: MetaMask reports that you are currently not on the Main Ethereum Network. Please select the Main Ethereum Network from the top-left dropdown menu in your MetaMask plugin, and try again. You will not be able to view contract information or access the Aequitas contract via this page until you do so.');
            } else {
                $("#contract-address").val(App.address).parent().removeClass('has-error').addClass('has-success');
                App.contract = web3.eth.contract(App.abi).at(App.address);
                return App.updateContractInfo(Object.keys(App.contract_properties)).then(resolve).catch(function (err) {
                    return reject(err)
                });
            }
        },
        bindEvents: function () {
            $(document).on('click', '.update', App.handleUpdateAddress);
            $(document).on('click', '#mine', function () {
                return new Promise(App.handleMine)
                    .then(function (success) {
                        $("#contract-error").hide();
                        $("#contract-success").show().html(success);
                    })
                    .catch(function (error) {
                        $("#contract-success").hide();
                        $("#contract-error").show().html(error);
                    });
            });
            $(document).on('click', '#update', function () {
                return new Promise(App.handleUpdate)
                    .then(function (success) {
                        $("#contract-error").hide();
                        $("#contract-success").show().html(success);
                    })
                    .catch(function (error) {
                        $("#contract-success").hide();
                        $("#contract-error").show().html(error);
                    });
            });
            $(document).on('click', '#refreshContractInfo', function () {
                return App.updateContractInfo(Object.keys(App.contract_properties))
                    .then(App.updateDisplay)
                    .then(Display.display)
                    .then(App.cleanupError)
                    .catch(App.handleError);
            });
            setInterval(function () {
                App.handleUpdateAddress();
                App.updateContractInfo(App.variable_contract_properties)
                    .then(App.updateDisplay)
                    .then(Display.updateCounter)
                    .then(App.cleanupError)
                    .catch(App.handleError);
            }, 60000);
            App.handleUpdateAddress();
        },
        handleNetworkCheck: function () {
            if (web3.version.network !== "1") {
                $("#contract-address").parent().removeClass('has-success').addClass('has-error');
                return false;
            }
            return true;
        },
        handleUpdateAddress: function (event) {
            if (event) {
                event.preventDefault();
            }
            let address = web3.eth.accounts[0];
            let field = $("#eth-address");
            field.val(address);
            if (!web3.isAddress(address)) {
                field.parent().removeClass('has-success').addClass('has-error');
                return false;
            } else {
                field.parent().removeClass('has-error').addClass('has-success');
                return true;
            }
        },
        handleContributionAmount: function () {
            let field = $("#contribution");
            let contributionAmount = parseFloat(field.val());
            if (isNaN(contributionAmount) || contributionAmount < 0.05) {
                field.parent().removeClass('has-success').addClass('has-error');
                return false;
            } else {
                field.parent().removeClass('has-error').addClass('has-success');
                return true;
            }
        },
        handleMine: function (resolve, reject) {
            event.preventDefault();
            let contributionAmountCheck = App.handleContributionAmount();
            let addressCheck = App.handleUpdateAddress();
            if (!contributionAmountCheck || !addressCheck) {
                return reject('<strong>Warning</strong>: Please check your contribution amount and address again fit the specified requirements before submitting.');
            }
            if (!App.handleNetworkCheck()) {
                return reject('<strong>Warning</strong>: MetaMask reports that you are currently not on the Main Ethereum Network. Please select the Main Ethereum Network from the top-left dropdown menu in your MetaMask plugin, and try again. You will not be able to view contract information or access the Aequitas contract via this page until you do so.');
            }
            App.contract.mine({
                from: web3.eth.accounts[0],
                value: web3.toWei(parseFloat($("#contribution").val()), "ether")
            }, function (failure, success) {
                if (failure) {
                    return reject('<strong>Warning</strong>: Error while sending transaction. Error received from node is as follows: <br><pre><code>' + failure + '</code></pre>');
                } else {
                    return resolve('<strong>Success!</strong> View your transaction on <a href="https://etherscan.io/tx/' + success + '">EtherScan</a>. Be advised that it may not show up immediately.');
                }
            });
        },
        handleUpdate: function (resolve, reject) {
            event.preventDefault();
            let addressCheck = App.handleUpdateAddress();
            if (!addressCheck) {
                return reject('<strong>Warning</strong>: Please check your account address fits the specified requirements before submitting.');
            }
            if (!App.handleNetworkCheck()) {
                return reject('<strong>Warning</strong>: MetaMask reports that you are currently not on the Main Ethereum Network. Please select the Main Ethereum Network from the top-left dropdown menu in your MetaMask plugin, and try again. You will not be able to view contract information or access the Aequitas contract via this page until you do so.');
            }

            App.contract.updateAccount({from: web3.eth.accounts[0]}, function (failure, success) {
                if (failure) {
                    return reject('<strong>Error</strong>: The signing of the transaction was not completed. Error received from node is as follows: <br><pre><code>' + failure + '</code></pre>');
                } else {
                    return resolve('<strong>Success!</strong> View your transaction on <a href="https://etherscan.io/tx/' + success + '">EtherScan</a>. Be advised that it may not show up immediately.');
                }
            });
        },
        updateContractInfo: function (property_list) {
            let assignProperties = function (property, result, resolve, reject, error) {
                if (error === null) {
                    App.contract_properties[property] = result;
                    resolve(result);
                } else {
                    reject('<strong>Warning</strong>: Could not connect to node. Check your Internet connection and try again. <br><pre><code>' + error + '</code></pre>');
                }
            };
            for (let property of property_list) {
                App.contract_properties[property] = new Promise(function (resolve, reject) {
                    if (property === "totalBlockContribution") {
                        App.contract.simulatedBlockNumber.call({from: web3.eth.accounts[0]}, function (error, blockNumber) {
                            if (error) {
                                reject('<strong>Warning</strong>: Could not connect to node. Check your Internet connection and try again. <br><pre><code>' + error + '</code></pre>');
                            } else {
                                App.contract[property](blockNumber.toNumber(), {from: web3.eth.accounts[0]}, function (error, result) {
                                    assignProperties(property, result, resolve, reject, error)
                                })
                            }
                        })
                    } else if (property === "pendingPayouts") {
                        let lastContributedBlock = null;
                        App.contract.pendingPayouts.call(web3.eth.accounts[0], {from: web3.eth.accounts[0]}, function (error, result) {
                            if (error) {
                                reject('<strong>Warning</strong>: Could not connect to node. Check your Internet connection and try again. <br><pre><code>' + error + '</code></pre>');
                            } else {
                                lastContributedBlock = parseInt(result[2]);
                                App.contract.totalBlockContribution.call(lastContributedBlock, {from: web3.eth.accounts[0]}, function (error, block) {
                                    assignProperties(property, result, resolve, reject, error);
                                    App.contract_properties.pendingPayouts.push(block);
                                });
                            }
                        });
                    } else if (property === "balanceOf") {
                        App.contract.balanceOf.call(web3.eth.accounts[0], {from: web3.eth.accounts[0]}, function (error, result) {
                            assignProperties(property, result, resolve, reject, error);
                        })
                    } else {
                        App.contract[property].call({from: web3.eth.accounts[0]}, function (error, result) {
                            assignProperties(property, result, resolve, reject, error)
                        });
                    }
                });
            }
            return Promise.all(Object.values(App.contract_properties));
        },
        updateDisplay: function () {
            let selector = $('#rewardValue');
            if (selector.length) {
                selector.data("to", parseInt(App.contract_properties.rewardValue) / (Math.pow(10, parseInt(App.contract_properties.decimals))));
            }
            selector = $('#simulatedBlockNumber');
            if (selector.length) {
                selector.data("to", App.contract_properties.simulatedBlockNumber);
            }
            selector = $('#timeSinceLastBlock');
            if (selector.length) {
                selector.data("to", parseInt((Date.now() / 1000 - parseInt(App.contract_properties.timeOfLastBlock)) / 60));
            }
            selector = $('#totalBlockContribution');
            if (selector.length) {
                selector.data("to", web3.fromWei(App.contract_properties.totalBlockContribution, 'Ether'));
            }
            selector = $('#totalSupply');
            if (selector.length) {
                selector.data("to", App.contract_properties.totalSupply / (Math.pow(10, parseInt(App.contract_properties.decimals))));
            }
            selector = $('#halveningTimeLeft');
            if (selector.length) {
                let rewardReductionRate = parseInt(App.contract_properties.rewardReductionRate);
                let currentBlockNumber = parseInt(App.contract_properties.simulatedBlockNumber);
                let blockInterval = parseInt(App.contract_properties.blockInterval) / 60;
                selector.data("to", parseInt((rewardReductionRate - currentBlockNumber % rewardReductionRate) * blockInterval / (60 * 24)));
            }
            selector = $('#myContribution');
            if (selector.length) {
                let contribution = parseInt(App.contract_properties.pendingPayouts[3]);
                selector.data("to", web3.fromWei(contribution, 'finney'))
            }
            selector = $('#myPendingPayouts');
            if (selector.length) {
                let contribution = parseInt(App.contract_properties.pendingPayouts[3]);
                let totalBlockContribution = parseInt(App.contract_properties.pendingPayouts[4]);
                let blockReward = parseInt(App.contract_properties.pendingPayouts[1]);
                selector.data("to", totalBlockContribution === 0 ? 0 : (contribution * blockReward / totalBlockContribution) / Math.pow(10, parseInt(App.contract_properties.decimals)))
            }
            selector = $('#balanceOf');
            if (selector.length) {
                let balance = parseInt(App.contract_properties.balanceOf);
                selector.data("to", balance / Math.pow(10, parseInt(App.contract_properties.decimals)))
            }
        },
        cleanupError: function () {
            $("#contract-error").hide();
            $("#contract-success").hide();
            $('#mine').show();
            $('#update').show();
        },
        handleError: function (err) {
            $('#mine').hide();
            $('#update').hide();
            let error_display = $("#contract-error");
            let success_display = $("#contract-success");
            error_display.show().html(err);
            success_display.hide();
        },
        contract_properties: {
            initialReward: null,
            simulatedBlockNumber: null,
            rewardScarcityFactor: null,
            rewardReductionRate: null,
            blockInterval: null,
            version: null,
            symbol: null,
            decimals: null,
            name: null,
            totalSupply: null,
            timeOfLastBlock: null,
            rewardValue: null,
            totalBlockContribution: null,
            pendingPayouts: null,
            balanceOf: null
        },
        variable_contract_properties: ['simulatedBlockNumber', 'totalSupply', 'timeOfLastBlock', 'rewardValue', 'totalBlockContribution', 'pendingPayouts', 'balanceOf'],
        address: "0x1EB96d45C1f8FfB2c583262cE5E536D0B38F160d",
        abi: [
            {
                "constant": true,
                "inputs": [],
                "name": "name",
                "outputs": [
                    {
                        "name": "",
                        "type": "string"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "name": "_spender",
                        "type": "address"
                    },
                    {
                        "name": "_value",
                        "type": "uint256"
                    }
                ],
                "name": "approve",
                "outputs": [
                    {
                        "name": "success",
                        "type": "bool"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "rewardValue",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "totalSupply",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "name": "_from",
                        "type": "address"
                    },
                    {
                        "name": "_to",
                        "type": "address"
                    },
                    {
                        "name": "_value",
                        "type": "uint256"
                    }
                ],
                "name": "transferFrom",
                "outputs": [
                    {
                        "name": "success",
                        "type": "bool"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "decimals",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint8"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "rewardScarcityFactor",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "version",
                "outputs": [
                    {
                        "name": "",
                        "type": "string"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "name": "totalBlockContribution",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [],
                "name": "updateAccount",
                "outputs": [],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "name": "_owner",
                        "type": "address"
                    }
                ],
                "name": "balanceOf",
                "outputs": [
                    {
                        "name": "balance",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [],
                "name": "withdrawEther",
                "outputs": [],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "name": "minedBlock",
                "outputs": [
                    {
                        "name": "",
                        "type": "bool"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "name": "",
                        "type": "address"
                    }
                ],
                "name": "pendingPayouts",
                "outputs": [
                    {
                        "name": "addr",
                        "type": "address"
                    },
                    {
                        "name": "blockPayout",
                        "type": "uint256"
                    },
                    {
                        "name": "lastContributionBlockNumber",
                        "type": "uint256"
                    },
                    {
                        "name": "blockContribution",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "owner",
                "outputs": [
                    {
                        "name": "",
                        "type": "address"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "symbol",
                "outputs": [
                    {
                        "name": "",
                        "type": "string"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [],
                "name": "mine",
                "outputs": [],
                "payable": true,
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "name": "_to",
                        "type": "address"
                    },
                    {
                        "name": "_value",
                        "type": "uint256"
                    }
                ],
                "name": "transfer",
                "outputs": [
                    {
                        "name": "success",
                        "type": "bool"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "initialReward",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "blockInterval",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "name": "_spender",
                        "type": "address"
                    },
                    {
                        "name": "_value",
                        "type": "uint256"
                    },
                    {
                        "name": "_extraData",
                        "type": "bytes"
                    }
                ],
                "name": "approveAndCall",
                "outputs": [
                    {
                        "name": "success",
                        "type": "bool"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "rewardReductionRate",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "simulatedBlockNumber",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "name": "_owner",
                        "type": "address"
                    },
                    {
                        "name": "_spender",
                        "type": "address"
                    }
                ],
                "name": "allowance",
                "outputs": [
                    {
                        "name": "remaining",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "timeOfLastBlock",
                "outputs": [
                    {
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "name": "newOwner",
                        "type": "address"
                    }
                ],
                "name": "transferOwnership",
                "outputs": [],
                "payable": false,
                "type": "function"
            },
            {
                "inputs": [],
                "payable": false,
                "type": "constructor"
            },
            {
                "payable": false,
                "type": "fallback"
            },
            {
                "anonymous": false,
                "inputs": [
                    {
                        "indexed": true,
                        "name": "_from",
                        "type": "address"
                    },
                    {
                        "indexed": true,
                        "name": "_to",
                        "type": "address"
                    },
                    {
                        "indexed": false,
                        "name": "_value",
                        "type": "uint256"
                    }
                ],
                "name": "Transfer",
                "type": "event"
            },
            {
                "anonymous": false,
                "inputs": [
                    {
                        "indexed": true,
                        "name": "_owner",
                        "type": "address"
                    },
                    {
                        "indexed": true,
                        "name": "_spender",
                        "type": "address"
                    },
                    {
                        "indexed": false,
                        "name": "_value",
                        "type": "uint256"
                    }
                ],
                "name": "Approval",
                "type": "event"
            }
        ]
    };

    App.init();
})
;