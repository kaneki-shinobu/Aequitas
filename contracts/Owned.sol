pragma solidity ^0.4.11;

/*
 * The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE
*/


// A base contract that implements the following concepts:
// 1. A smart contract with an owner
// 2. Methods that can only be called by the owner
// 3. Transferability of ownership
contract Owned {
    // The address of the owner
    address public owner;

    // Constructor
    function Owned() {
        owner = msg.sender;
    }

    // A modifier that provides a pre-check as to whether the sender is the owner
    modifier _onlyOwner {
        if (msg.sender != owner) revert();
        _;
    }

    // Transfers ownership to newOwner, given that the sender is the owner
    function transferOwnership(address newOwner) _onlyOwner {
        owner = newOwner;
    }
}